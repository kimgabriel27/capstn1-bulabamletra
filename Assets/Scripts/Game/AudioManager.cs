using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    CurrentWordUIScript WordScript;
    MainGameScript MainScript;

    GameObject TargetWord;
    AudioSource Audio;
    AudioClip TargetClip;
    // Start is called before the first frame update
    void Start()
    {
        WordScript = CurrentWordUIScript.Instance;
        MainScript = MainGameScript.GetInstance;
        Audio = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnAudioButtonPress()
    {
        Debug.Log(MainScript.TargetWord);

        TargetWord = MainScript.GetComponent<MainGameScript>().RandomWord;
        TargetClip = TargetWord.GetComponent<AudioSource>().clip;

        Audio.PlayOneShot(TargetClip);
    }
}
