using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScript : MonoBehaviour
{
    MainGameScript MScript;
    // Start is called before the first frame update
    void Start()
    {
        MScript = MainGameScript.GetInstance;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Letter")
        {
            MScript.ShowLoseScreen();
            MScript.blackBG.SetActive(true);
            GameEvents.current.PlayerLose();
        }
    }
}
