using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class MainGameScript : MonoBehaviour
{
    MainGameScript() { }

    public static MainGameScript GetInstance
    {
        get
        {
            if (Instance == null)
            {
                Instance = GameObject.FindObjectOfType<MainGameScript>();
                Debug.Log("Instance Made");
            }
            return Instance;
        }
    }

    private static MainGameScript Instance;
    public static int CurrentScore = 0;

    ObjectPoolScript LetterPool;
    WordManager WManager;
    PauseMenu PMenu;
    private PlayerScript player;

    //Public Variables
    public string TargetWord;
    public string CurrentWord;

    public string CWord; // C-word D:
    public static bool SettingsActive = false;
    public GameObject SettingsUI;
    public int MaxCWNumber;
    public int CWNumber;

    public GameObject WrongScreen;
    public GameObject LoseScreen;
    public GameObject WinScreen;
    public GameObject blackBG;
    public GameObject ContinueOnClick;
    public GameObject RandomWord;
    public Transform SpawnPoint;

    //GameObjects of CurrentWords
    public GameObject WordObjects;
    public GameObject Player;

    //GridSpawning Variables
    public GameObject spawnThis;
    public int x = 5;
    public int y = 5;
    public float XAdjustment;
    public float YAdjustment;
    public float radius = 0.5f;
    public bool useAsInnerCircleRadius = true;

    //Private Variables
    private string Word = "";
    private float offsetX, offsetY;

    public char[] LetterArray;
    public List<string> LetterList;
    public List<int> RandomNumbers;
    public List<string> TargetWordList;

    //configurable number
    public int AmountOfLetters;
    int LineCounter;
    public int SpawnerCounter;
    int LetterCounter;

    // Start is called before the first frame update
    private void Start()
    {
        Instance = this;
        LetterPool = ObjectPoolScript.Instance;
        WManager = WordManager.Instance;
        PMenu = PauseMenu.Instance;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>();
        GameEvents.current.OnLetterSpawn += SpawnLetter;
        GameEvents.current.OnLetterScoreEvent += ScoreLetter;
      
        CurrentScore = 0;
        CWNumber = 0;
        LineCounter = 0;
        SpawnerCounter = 0;

        NewList();
        GetRandomWords();

        GameEvents.current.LoadText();

        CurrentWord = Word;

        //Grid Spawning
        float unitLength = (useAsInnerCircleRadius) ? (radius / (Mathf.Sqrt(3) / 2)) : radius;
        offsetX = unitLength * Mathf.Sqrt(3);
        offsetY = unitLength * 1.5f;
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {   
                Vector2 hexpos = HexOffset(i, j);
                Vector3 pos = new Vector3(hexpos.x, hexpos.y, 0);
                Instantiate(spawnThis, pos, Quaternion.identity);   
            }
        }

        LetterArray = TargetWord.ToCharArray();

        Player.GetComponent<PlayerScript>().PlayerLetterArray = LetterArray;
        //cycle letters
        Player.GetComponent<PlayerScript>().MovePrev2ToShoot();
        Player.GetComponent<PlayerScript>().MovePrev3To2();
        Player.GetComponent<PlayerScript>().GetRandomBullet();

        Instantiate(RandomWord, SpawnPoint.transform.position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        //if (CurrentWord == TargetWord)
        //{
        //    ShowWinScreen();
        //}
    }

    void NewList()
    {
        for (int i = 0; i < AmountOfLetters; i++)
        {
            RandomNumbers.Add(Random.Range(0, LetterList.Count - 1));
        }

        RandomNumbers.Distinct().ToList();

        Player.GetComponent<PlayerScript>().PlayerRandomNumbers = RandomNumbers;
    }

    public void ClearButton()
    {
        CurrentWord = "";
    }

    public void EraseButton()
    {
        CurrentWord = CurrentWord.Remove(CurrentWord.Length - 1);
    }

    public void ShowWinScreen()
    {
       
        if (CheckWordsForMistakes())
        {
            ///LoseScreen.SetActive(true);
            GameEvents.current.WrongWord();
            CurrentWord = "";
         
        }
        else
        {
            PMenu.GamePaused = true;
            WinScreen.SetActive(true);
            blackBG.SetActive(true);
            ContinueOnClick.SetActive(true);
        }
    
    }

    public void ShowLoseScreen()
    {
        LoseScreen.SetActive(true);
        //ContinueOnClick.SetActive(true);
    }

    public bool CheckWordsForMistakes()
    {
        /*
        for (int i = 0; i < MaxCWNumber; i++)
        {
            if (CurrentWord[i] != TargetWord[i])
            {
                Debug.Log(CurrentWord[i]);
                Debug.Log(TargetWord[i]);
                return true;
            }
        }
        */
        if (CurrentWord == "")
        {
            StartCoroutine(ShowMaliScreen());
            return true;
        }

        if (CurrentWord != TargetWord)
        {
            StartCoroutine(ShowMaliScreen());
            return true;
        }

        return false;
    }

    private void GetRandomWords()
    {
        int N = WManager.GetComponent<WordManager>().Words.Count;
        int RandomWordNumber = Random.Range(0, N - 1);

        int RandomNumber;
        switch (WManager.GameDifficulty)
        {
            case 0:
               
                Debug.Log("easy");
                RandomNumber = Random.Range(0, WManager.EasyWords.Count);
                //TargetWord.Add(WManager.Easy[RandomNumber].Word);
                //TargetWord.Add(WManager.EasyWords[RandomNumber]);
                RandomWordNumber = Random.Range(0, WManager.GetComponent<WordManager>().EWords.Count);

                RandomWord = WManager.GetComponent<WordManager>().EWords[RandomWordNumber];
                TargetWord = WManager.GetComponent<WordManager>().EWords[RandomWordNumber].name;
                //WManager.RemoveWordFromList(RandomNumber);
                break;

            case 1:
                Debug.Log("ave");

                RandomNumber = Random.Range(0, WManager.AveWords.Count);
                //TargetWord.Add(WManager.AveWords[RandomNumber]);
                RandomWordNumber = Random.Range(0, WManager.GetComponent<WordManager>().MWords.Count);
                RandomWord = WManager.GetComponent<WordManager>().MWords[RandomWordNumber];
                TargetWord = WManager.GetComponent<WordManager>().MWords[RandomWordNumber].name;
                //WManager.RemoveWordFromList(RandomNumber);
                break;

            case 2:
                Debug.Log("hard");
                RandomNumber = Random.Range(0, WManager.HardWords.Count);
                //TargetWord.Add(WManager.HardWords[RandomNumber]);
                RandomWordNumber = Random.Range(0, WManager.GetComponent<WordManager>().HWords.Count);
                RandomWord = WManager.GetComponent<WordManager>().HWords[RandomWordNumber];
                TargetWord = WManager.GetComponent<WordManager>().HWords[RandomWordNumber].name;

              //  WManager.RemoveWordFromList(RandomNumber);
                break;

            default:
                break;
        }


      
       // int RandomWordNumber = Random.Range(0, N - 1);

        // RandomWord = WManager.GetComponent<WordManager>().Words[RandomWordNumber];
        //TargetWord = WManager.GetComponent<WordManager>().Words[RandomWordNumber].name;
    }

    private void ScoreLetter()
    {
        CurrentScore++;
    }

    Vector2 HexOffset(float x, float y)
    {
        x += XAdjustment;
        y += YAdjustment;

        Vector2 position = Vector2.zero;

        if (y % 2 == 0)
        {
            position.x = x * offsetX;
            position.y = y * offsetY;
        }
        else
        {
            position.x = (x + 0.5f) * offsetX;
            position.y = y * offsetY;
        }

        return position;
    }

    private void SpawnLetter(GameObject Spawn) //
    {
        //abtedilu

        //y = 30
        //int counter
        //int LIN = letters in word
        //int line counter = 30/LIN
        //if word[counter] == "A" then 

        
        Debug.Log("Spawning letter");
        int SpawnerRandomNumber;
        SpawnerRandomNumber = Random.Range(0,100);

       
        if(SpawnerRandomNumber >= 95)
        {
            for (int i = 0; i < TargetWord.Length; i++)
            {
                char c = TargetWord[i];
                LetterPool.SpawnFromPool(c.ToString(), Spawn.transform.position, Quaternion.identity);
                SpawnerCounter++;
            }
            //LetterPool.SpawnFromPool(LetterArray[LineCounter].ToString(), Spawn.transform.position, Quaternion.identity);
            SpawnerCounter++;
        }
        else
        {
            //if (SpawnerRandomNumber >= 50)
            //{
            //    char c = TargetWord[Random.Range(0, TargetWord.Length)];
            //    LetterPool.SpawnFromPool(c.ToString(), Spawn.transform.position, Quaternion.identity);
            //    SpawnerCounter++;
            //}
            //else
            //{
            //    LetterPool.SpawnFromPool(LetterList[RandomNumbers[Random.Range(0, RandomNumbers.Count)]], Spawn.transform.position, Quaternion.identity);
            //    SpawnerCounter++;
            //}
            char c = TargetWord[Random.Range(0, TargetWord.Length)];
            LetterPool.SpawnFromPool(c.ToString(), Spawn.transform.position, Quaternion.identity);
            SpawnerCounter++;
            //LetterPool.SpawnFromPool(LetterList[RandomNumbers[Random.Range(0, RandomNumbers.Count)]], Spawn.transform.position, Quaternion.identity);
        }

        if (SpawnerCounter >= x * 3)
        {
            LineCounter++;
        }

        //if (LineCounter > LetterArray.Length - 1)
        //{
        //    LineCounter = 0;
        //}
    }

    IEnumerator ShowMaliScreen()
    {
        player.canShoot = false;
        WrongScreen.SetActive(true);
        yield return new WaitForSeconds(1);
        WrongScreen.SetActive(false);
        player.canShoot = true;
    }
}
