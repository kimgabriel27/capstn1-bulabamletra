using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterSpawnScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.current.SpawnLetter(gameObject);
        gameObject.SetActive(false);
    }

}
