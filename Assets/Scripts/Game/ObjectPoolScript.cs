using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolScript : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject LetterPrefab;
        public int size;
    }

    public static ObjectPoolScript Instance;

    private void Awake()
    {
        Instance = this;
    }

    public List<Pool> Letters;
    public Dictionary<string, Queue<GameObject>> PoolDictionary;

    // Start is called before the first frame update
    void Start()
    {
        PoolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in Letters)
        {
            Queue<GameObject> letter = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.LetterPrefab);
                obj.SetActive(false);
                letter.Enqueue(obj);
            }

            PoolDictionary.Add(pool.tag, letter);
        }
    }

    public GameObject SpawnFromPool(string tag, Vector2 position, Quaternion rotation)
    {
        if (!PoolDictionary.ContainsKey(tag))
        {
            Debug.LogWarning(tag);
            return null;
        }

        GameObject LetterToSpawn = PoolDictionary[tag].Dequeue();
        LetterToSpawn.SetActive(true);
        LetterToSpawn.transform.position = position;
        LetterToSpawn.transform.rotation = rotation;

        InterfacePooledObject PooledLetter = LetterToSpawn.GetComponent<InterfacePooledObject>();

        if (PooledLetter != null)
        {
            PooledLetter.OnObjectSpawn();
        }

        PoolDictionary[tag].Enqueue(LetterToSpawn);

        return LetterToSpawn;
    }
}
