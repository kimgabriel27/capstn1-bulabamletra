using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public GameObject bubble;
    public Vector3 initPos;
    public int columns;
    public int rows;
    public GameObject[,] grid;
    public float gap;

    const int COL_MAX = 12;
    const int ROW_MAX = 20;

    // Start is called before the first frame update
    void Start()
    {
        grid = new GameObject[COL_MAX, ROW_MAX];

        for (int r = 0; r < rows; r++)
        {
            if (r % 2 != 0) columns -= 1;
            for (int c = 0; c < columns; c++)
            {
                Vector3 position = new Vector3((float)c * gap, (float)(-r) * gap, 0f) + initPos;
                if (r % 2 != 0)
                    position.x += 0.5f * gap;

                int newKind = 0;//letter

                Create(position, newKind);
                //levelpos++;
            }
            if (r % 2 != 0) columns += 1;
        }
    }

    public Vector3 Snap(Vector3 position)
    {
        Vector3 objectOffset = position - initPos;
        Vector3 objectSnap = new Vector3(
            Mathf.Round(objectOffset.x / gap),
            Mathf.Round(objectOffset.y / gap),
            0f
        );

        if ((int)objectSnap.y % 2 != 0)
        {
            if (objectOffset.x > objectSnap.x * gap)
            {
                objectSnap.x += 0.5f;
            }
            else
            {
                objectSnap.x -= 0.5f;
            }
        }
        return initPos + objectSnap * gap;
    }

    public GameObject Create(Vector2 position, int kind)
    {

        Vector3 snappedPosition = Snap(position);
        int row = (int)Mathf.Round((snappedPosition.y - initPos.y) / gap);
        int column = 0;
        if (row % 2 != 0)
        {
            column = (int)Mathf.Round((snappedPosition.x - initPos.x) / gap - 0.5f);
        }
        else
        {
            column = (int)Mathf.Round((snappedPosition.x - initPos.x) / gap);
        }


        GameObject bubbleClone = (GameObject)Instantiate(bubble, snappedPosition, Quaternion.identity);
        try
        {
            grid[column, -row] = bubbleClone;
        }
        catch (System.IndexOutOfRangeException)
        {
            Destroy(bubbleClone);
            return null;
        }

        CircleCollider2D collider = bubbleClone.GetComponent<CircleCollider2D>();
        if (collider != null)
        {
            collider.isTrigger = true;
        }

        //GridMember gridMember = bubbleClone.GetComponent<GridMember>();
        //if (gridMember != null)
        //{

        //    gridMember.parent = gameObject;
        //    gridMember.row = row;
        //    gridMember.column = column;
        //    if (kind == 6)
        //    {
        //        gridMember.kind = (int)Random.Range(1f, 6f);
        //    }
        //    else
        //    {
        //        gridMember.kind = kind;
        //    }

        //    SpriteRenderer spriteRenderer = bubbleClone.GetComponent<SpriteRenderer>();
        //    if (spriteRenderer != null)
        //    {
        //        Color[] colorArray = new Color[] { Color.red, Color.cyan, Color.yellow, Color.green, Color.magenta };
        //        spriteRenderer.color = colorArray[gridMember.kind - 1];
        //    }
        //}
        bubbleClone.SetActive(true);

        //if (column == 6 && row == -6 && youLose != null)
        //    youLose.SetActive(true);

        try
        {
            grid[column, -row] = bubbleClone;
        }
        catch (System.IndexOutOfRangeException)
        {
        }

        return bubbleClone;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
