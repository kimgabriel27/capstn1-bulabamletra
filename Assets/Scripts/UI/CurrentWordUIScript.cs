using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentWordUIScript : MonoBehaviour
{
    public static CurrentWordUIScript Instance;

    MainGameScript MainScript;
    WordManager WManager;

    public Text CurrentWordText;
    public string TargetWord;
    public int WordNumber;

    public string CurrentText;

    public GameObject Highlight;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;

        MainScript = MainGameScript.GetInstance;
        WManager = WordManager.Instance;
        CurrentWordText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (MainScript.CWNumber == WordNumber)
        {
            Highlight.SetActive(true);
        }
        else
        {
            Highlight.SetActive(false);
        }

        if (gameObject.activeInHierarchy)
        {
            CurrentWordText.text = MainScript.CurrentWord.ToString();
            TargetWord = MainScript.TargetWord.ToString();

            if (CurrentWordText.text == TargetWord)
            {
                Debug.Log("Complete");
            }
        }
    }
}
