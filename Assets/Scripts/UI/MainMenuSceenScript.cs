using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuSceenScript : MonoBehaviour
{
    public GameObject CreditsScreen;
    public GameObject CreditsButton;
    public GameObject PlayButton;

    WordManager WManager;

    private void Start()
    {
        WManager = WordManager.Instance;
    }

    private void Update()
    {
    }

    public void LoadCredits()
    {
        CreditsScreen.SetActive(true);
    }

    public void SelectDifficulty(GameObject Panel)
    {
        Panel.SetActive(!Panel.activeInHierarchy);
    }

    public void LoadMainGame(int DifficultyNumber)
    {
        WordManager.Instance.SetDifficulty(DifficultyNumber);
        SceneManager.LoadScene("MainGame", LoadSceneMode.Single);
    }
}
