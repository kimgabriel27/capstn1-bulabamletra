using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLoseScript : MonoBehaviour
{

    PauseMenu PauseScript;
    public GameObject WinScreen;
    public GameObject LoseScreen;
    public GameObject blackBG;

    // Start is called before the first frame update
    void Start()
    {
        PauseScript = PauseMenu.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (WinScreen.activeInHierarchy || LoseScreen.activeInHierarchy)
        {
            gameObject.SetActive(true);
            blackBG.SetActive(true);
            PauseScript.GamePaused = true;
        }

    }

    public void ClickToContinue()
    {
        if (WinScreen.activeInHierarchy)
        {

            WinScreen.SetActive(false);
            SceneManager.LoadScene("MainMenu");
        }

        if (LoseScreen.activeInHierarchy)
        {
            LoseScreen.SetActive(false);
        }

        PauseScript.GamePaused = false;
        gameObject.SetActive(false);
    }
}
