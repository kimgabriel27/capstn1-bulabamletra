using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    ObjectPoolScript LetterPool;
    WordManager WManager;
    PauseMenu PauseScript;
    MainGameScript mainGame;
    LineRenderer lr;
    Vector2 StartLineVector;
    Vector2 EndLineVector;

    //Temporary fix for lose condition
    [SerializeField] private GameObject border;
    private Coroutine borderCoroutine;

    public GameObject BulletSpawnPoint;
    public GameObject BulletPreview;
    public GameObject BulletPreview2;
    public GameObject BulletPreview3;

    public char[] PlayerLetterArray;
    public List<string> PlayerLetterList;
    public List<int> PlayerRandomNumbers;

    GameObject LetterToShoot;
    GameObject Prev2;
    GameObject Prev3;


    public bool canShoot = true;

    // Start is called before the first frame update
    void Start()
    {
        LetterPool = ObjectPoolScript.Instance;
        WManager = WordManager.Instance;
        PauseScript = PauseMenu.Instance;
        mainGame = GameObject.FindGameObjectWithTag("MainGame").GetComponent<MainGameScript>();

        lr = GetComponent<LineRenderer>();

        // lr.positionCount = 2;
        //  lr.SetVertexCount(2);
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseScript.GamePaused || MainGameScript.GetInstance.LoseScreen.activeSelf)
        {
            Debug.Log("Paused");
            return;
        }
    
        Vector3 mouseScreen = Input.mousePosition;
        Vector3 mouse = Camera.main.ScreenToWorldPoint(mouseScreen);

        transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(mouse.y - transform.position.y, mouse.x - transform.position.x) * Mathf.Rad2Deg - 90);

        StartLineVector = transform.position;
        EndLineVector = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonUp(0) && mouse.x < 0 && mouse.y > -3 && canShoot)
        {
            MovePrev2ToShoot();
            MovePrev3To2();
            GetRandomBullet();

            ShootLetter();

            // Temporary fix for lose condition
            if (borderCoroutine != null)
                StopCoroutine(borderCoroutine);
            borderCoroutine = StartCoroutine(DisableBorder());
        }

        //  lr.SetPosition(0, StartLineVector);
        //   lr.SetPosition(1, EndLineVector);
    }

    // Temporary fix for lose condition
    private IEnumerator DisableBorder()
    {
        float duration = 0.75f;
        float currTime = 0;

        // border.SetActive(false);

        while (currTime < duration)
        {
            currTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        border.SetActive(true);

    }

    public void MovePrev2ToShoot()
    {
        if (Prev2)
        {
            Prev2.transform.position = BulletPreview.transform.position;
            LetterToShoot = Prev2;
           
        }
    }

    public void MovePrev3To2()
    {
        if (Prev3)
        {
            Prev3.transform.position = BulletPreview2.transform.position;
            Prev2 = Prev3;
        }
    }

    public void GetRandomBullet()
    {
        int SpawnerRandomNumber;
        SpawnerRandomNumber = Random.Range(0, 100);
        char c = mainGame.TargetWord[Random.Range(0, mainGame.TargetWord.Length)];
        if (SpawnerRandomNumber >= 15)
        {

            //Prev3 = LetterPool.SpawnFromPool(PlayerLetterArray[Random.Range(0, PlayerLetterArray.Length)].ToString(), BulletPreview3.transform.position, Quaternion.identity);
            Prev3 = LetterPool.SpawnFromPool(c.ToString(), BulletPreview3.transform.position, Quaternion.identity);
        }
        else
        {
            Prev3 = LetterPool.SpawnFromPool(PlayerLetterList[PlayerRandomNumbers[Random.Range(0, PlayerRandomNumbers.Count)]], BulletPreview3.transform.position, Quaternion.identity);
        }
        
        Prev3.name = "Preview";
    }

    void ShootLetter()
    {
        if (LetterToShoot)
        {
            Debug.Log("Shoot Bullet");
            LetterToShoot.name = "Bullet";
            //LetterToShoot.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            LetterToShoot.transform.position = BulletSpawnPoint.transform.position;
            LetterToShoot.transform.rotation = BulletSpawnPoint.transform.rotation;
            Rigidbody2D rb = LetterToShoot.GetComponent<Rigidbody2D>();
            rb.velocity = transform.up * 10;
            rb.isKinematic = false;
            canShoot = false;
        }
    }
}
