using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    Rigidbody2D BulletBody;
    SpriteRenderer BulletSpriteRenderer;

    public GameObject BulletSprite;
    public float BulletSpeed;

    // Start is called before the first frame update
    void Start()
    {
        BulletBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        BulletBody.velocity = transform.up * BulletSpeed;
    }

    void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ////lettera||letterb.,..
        if (collision.collider.gameObject.name == "Letter" || collision.collider.gameObject.name == "Border")
        {
            Destroy(this.gameObject);
        }
    }
}
