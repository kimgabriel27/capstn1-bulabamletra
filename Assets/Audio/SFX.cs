using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    public AudioSource hoverSound;
    public AudioSource clickSound;
    // Start is called before the first frame update
    public void onMouseHover()
    {
        hoverSound.Play();
    }

    public void OnClickSound()
    {
        clickSound.Play();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
